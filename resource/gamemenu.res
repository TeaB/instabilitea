"GameMenu"
{
	"1"
	{
		"label" "CONTINUE"
		"command" "ResumeGame"
		"InGameOrder" "10"
		"OnlyInGame" "1"
	}
	"5"	
	{
		"label" "PLAY"
		"command" "engine map instabilitea"
		"InGameOrder" "20"
		"notmulti" "1"
	}
	"9"
	{
		"label" "OPTIONS"
		"command" "OpenOptionsDialog"
		"InGameOrder" "50"
	}
	"10"
	{
		"label" "CREDITS"
		"command" "engine map instabilitea_background"
		"InGameOrder" "60"
	}
	//"11"
	//{
	//	"label" "BACK TO MAIN MENU"
	//	"command" "engine map_background instabilitea_background"
	//	"InGameOrder" "70"
	//	"OnlyInGame" "1"
	//}
	"12"
	{
		"label" "QUIT"
		//"command" "quit"
		"command" "engine map_background instabilitea_background"
		"InGameOrder" "100"
	}
}

