# INSTALLATION

(in)stabilitea is a [Source Engine](https://en.wikipedia.org/wiki/Source_(game_engine)) Mod, so you will need to install a Version of Source Engine to run it with. It comes free with a Steam Account.

## Default Install
1. Install [Source SDK Base 2013 Singleplayer](steam://rungameid/243730) in Steam.
2. Right click on it and under `Properties > Betas` select `upcoming` instead of none.
3. Move the `instabilitea` folder into your `C:\Program Files (x86)\Steam\steamapps\sourcemods` folder (depends where your Steam is located).
4. Restart Steam.
5. Find and launch "(in)stabilitea" from the Steam Library.

That's it. But if you want to play it with a Controller or make a desktop shortcut, keep reading.

---

## Alternative Install (with Controller Support)
This version has the new Menu & Controller Support (thanks to @JoshuaAshton). You have to own Half-Life 2: Episode 2.
1. Install [Half-Life 2: Episode Two](steam://run/420) in Steam.
2. Move the `instabilitea` folder into your `C:\Program Files (x86)\Steam\steamapps\sourcemods` folder (depends where your Steam is located).
3. Delete the `bin` folder and rename the `bin-hl2` folder to `bin`.
4. Delete `gameinfo.txt` and rename `gameinfo-hl2.txt` to `gameinfo.txt`.
5. Restart Steam.
6. "(in)stabilitea" should be in the Steam Library (don't launch it directly).
7. Look for "Half-Life 2: Episode Two" in the Steam Library.
8. Add the parameters `-gamepadui -game "C:\Program Files (x86)\Steam\steamapps\sourcemods\instabilitea"` (adjust if on a different drive).
9. Launch "Half-Life 2: Episode Two".

## Making a Desktop Shortcut
1. in Steam Library: `Right-click > Manage > Add desktop shortcut`).
2. On desktop: `Right-click > Properties > Web Document > Change Icon`
3. Then locate the icon in the sourcemod folder).
4. (if Alternative Method was used) rename it to "(in)stabilitea".
