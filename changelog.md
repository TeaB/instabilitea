## TLDR: changelog

Compared to the [Map Labs](https://www.moddb.com/mods/map-labs/downloads/map-lab-9-back-on-track) and [Tea’s Map Pack](https://teamakesgames.itch.io/teas-map-pack-1) release, a few things were changed:

- [x] 🍹 20 collectible soda cans
- [x] 😎 accessible mode (for people sensitive to epileptical seizures & motion sickness)
- [x] 🎮 controller support
- [x] 🖥️ UI custom colors/fonts/Icons ...
- [x] 📜 credits
- [x] 👀 new easter eggs

Bugs fixed
- [x] 🚪 no longer get stuck in a door in the last puzzle
- [x] 🧩 puzzles easier to read & more satisfying to execute
- [x] 🎵 music volume works now

Visual
- [x] ▶️ pretty GamepadUI menu
- [x] ✨ texture adjustments (new vending machines, paintings, projector, better reflections)
- [x] 💡 lighting adjustments
- [x] 🎧 a few new sounds and background music


# DEV NOTES
## How controller support COULD have worked but does NOT
- ❌ `sc_force_action_manifest hl2`,  doesn't seem to work.
- ❌ `sc_enable 1` - doens't do anything (is 1 by default)
- ❌ `sc_controller_mode 1` - doesn't do anything (is 0 by default)
- ❌ all of the above in `autoexec.cfg` (still is) doesn't work.
- ❌ copy `steam_input/action_manifest_hl2.vdf` and editing it to be `instabilitea`-specific instead of `HL2`-specific, then `sc_force_action_manifest instabilitea` (it still tries to load from the HL2 folder, not mod folder, and even if I put the file into HL2 gamedir, I couldn't get it to work - but it woudld be an acceptable solution in my usecase!)