"MainMenu"
{	
	"ResumeGame"
	{
		"text"			"CONTINUE" // #GameUI_GameMenu_ResumeGame
		"command"		"cmd gamemenucommand resumegame"
		"priority"		"7"
		"family"		"ingame"
	}
	
	"NewGame"
	{
		"text"			"PLAY" // #GameUI_GameMenu_NewGame
		//"command"		"cmd gamepadui_opennewgamedialog"
		"command"		"cmd map instabilitea"
		"priority"		"6"
		"family"		"all"
	}
	
	//"BonusMaps"
	//{
	//	"text"			"OTHER MAPS" // #GameUI_GameMenu_BonusMaps
	//	"command"		"cmd gamepadui_openbonusmapsdialog"
	//	"priority"		"5"
	//	"family"		"all"
	//}
	
	"Options"
	{
		"text"			"OPTIONS"
		"command"		"cmd gamepadui_openoptionsdialog"
		"priority"		"4"
		"family"		"all"
	}
	
	"Credits"
	{
		"text"			"CREDITS"
		"command"		"cmd map instabilitea_background"
		"priority"		"3"
		"family"		"all"
	}

	"Quit"
	{
		"text"			"QUIT"
		//"command"		"cmd gamepadui_openquitgamedialog"
		"command"		"cmd map_background instabilitea_background"
		"priority"		"2"
		"family"		"ingame"
	}
	
	"Exit"
	{
		"text"			"QUIT"
		"command"		"cmd gamepadui_openquitgamedialog"
		"priority"		"1"
		"family"		"mainmenu"
	}
}