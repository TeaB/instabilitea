"SchemeMainmenu"
{
	"BaseSettings"
	{
		//"Logo.Image"							"gamepadui/instabilitea_typography_logo"
		//"Logo.Image.Width"					"512"
		//"Logo.Image.Height"					"341"
		"Logo.OffsetX"							"85"
		"Logo.OffsetY.MainMenu"					"290"
		"Logo.OffsetY.InGame"					"340"
	
		"Buttons.OffsetX"						"85"
		"Buttons.OffsetY.MainMenu"				"95"
		"Buttons.OffsetY.InGame"				"70"
		"Buttons.Space"							"6"
	
		"Button.Width.Out"						"260"
		"Button.Width.Over"					"260"
		"Button.Width.Pressed"					"260"

		"Button.Height.Out"					"30"
		"Button.Height.Over"				"36"
		"Button.Height.Pressed"				"36"

		"FooterButtons.OffsetX"					"64"
		"FooterButtons.OffsetY"					"48"
		"FooterButtons.Spacing"					"8"

		"Button.Text.OffsetX.Out"				"8"
		"Button.Text.OffsetX.Over"				"18"
		"Button.Text.OffsetX.Pressed"			"18"
		"Button.Text.OffsetY.Out"				"0"
		"Button.Text.OffsetY.Over"				"0"
		"Button.Text.OffsetY.Pressed"			"0"

		"Button.Text.LeftBorder.Out"			"0"
		"Button.Text.LeftBorder.Over"			"8"
		"Button.Text.LeftBorder.Pressed"		"8"

		"Button.Description.OffsetX"			"1"
		"Button.Description.OffsetY"			"-3"

		"Button.Description.Hide.Out"			"1"
		"Button.Description.Hide.Over"			"1"
		"Button.Description.Hide.Pressed"		"1"

		"Button.Text.OffsetX.Animation.Duration"		"0.15"
		"Button.Width.Animation.Duration"				"0.15"
		"Button.Height.Animation.Duration"				"0.25"
		"Button.Background.Animation.Duration"			"0.2"
		"Button.Text.Animation.Duration"				"0.2"
		"Button.Description.Animation.Duration"			"0.5"
	}

	"Colors"
	{
		"Logo"									"255 255 255 255"
		
		"Button.Background.Out"					"0 0 0 0"
		"Button.Background.Over"				"10 12 24 120" // dark purple
		"Button.Background.Pressed"				"91 54 221 255"

		"Button.Background.LeftBorder.Out"			"0 0 0 255"
		"Button.Background.LeftBorder.Over"			"164 105 255 255" // bright purple
		"Button.Background.LeftBorder.Pressed"		"91 54 221 255" // middle purple

		"Button.Text.Out"						"224 224 227 255" // almost-white
		"Button.Text.Over"						"164 105 255 255" // bright purple
		"Button.Text.Pressed"					"10 12 24 255"

		"Button.Description.Out"				"0 0 0 0"
		"Button.Description.Over"				"0 0 0 0"
		"Button.Description.Pressed"			"0 0 0 0"
	}
	
	"Fonts"
	{
		"Logo.Font"
		{
			"settings"
			{
				"name"			"Tea-DIN"
				"tall"			"63" // default: 34
				"weight"		"400"
				"antialias"		"1"
			}
		}
		
		"Button.Text.Font"
		{
			"settings"
			{
				"name"			"D-DIN"
				"tall"			"25"
				"weight"		"400"
				"antialias"		"1"
			}
		}

		"Button.Text.Font.Over"
		{
			"settings"
			{
				"name"			"D-DIN"
				"tall"			"25"
				"weight"		"700"
				"antialias"		"1"
			}
		}
	}

	"CustomFontFiles"
	{
		"file"		"resource/HALFLIFE2.ttf"
		"file"		"gamepadui/fonts/din1451alt.ttf"
		"file"		"gamepadui/fonts/D-DIN.otf"
		"file"		"gamepadui/fonts/D-DIN-Bold.otf"
		"file"		"gamepadui/fonts/Tea-DIN-Condensed-Bold.otf"
	}
}
